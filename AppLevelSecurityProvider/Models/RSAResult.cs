﻿namespace TenantSecurityProvider.Models;

public class RsaResult
{
    public string Id { get; set; }

    public string Value { get; set; }
}
