﻿namespace TenantSecurityProvider.Models;

public class OrganizationMemberDicResult
{
    public Dictionary<int, List<OrganizationMemberInfo>> OrganizationMemberDic { get; set; }
    
    public DateTime VersionDate { get; set; }
}
