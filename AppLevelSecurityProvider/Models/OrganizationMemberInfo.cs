﻿using Newtonsoft.Json;

namespace TenantSecurityProvider.Models;

public class OrganizationMemberInfo
{
    public int ID { get; set; }

    public int OrganizationID { get; set; }

    public string UserName { get; set; }

    public bool IsWindowsUser { get; set; }

    public bool IsLeader { get; set; }

    //为了保持兼容性
    //[Obsolete("Role is deprecated, please use OrganizationRoles instead.")]
    public int? Role
    {
        get => OrganizationRoles?.Cast<int?>().FirstOrDefault();
        set
        {
            if (value != null)
            {
                OrganizationRoles = new[] { value.Value };
            }
        }
    }

    private IEnumerable<int> _organizationRoles;

    [JsonProperty(ObjectCreationHandling = ObjectCreationHandling.Replace)]
    public IEnumerable<int> OrganizationRoles
    {
        get => _organizationRoles ?? Array.Empty<int>();
        set => _organizationRoles = value;
    }
}
