﻿using GrapeCity.Forguncy.SecurityProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLevelSecurityProvider.Models
{
    public class OrganizationExt: Organization
    {
        public int ID { get; set; }
    }
}
