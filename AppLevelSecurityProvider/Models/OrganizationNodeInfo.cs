﻿using Newtonsoft.Json;

namespace TenantSecurityProvider.Models;

public class OrganizationNodeInfo
{
    [JsonProperty("id")]
    public int ID { get; set; }

    [JsonProperty("pId")]
    public int ParentID { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("nodeLevelId")]
    public int NodeLevelID { get; set; }

    [JsonProperty("nodeOrder")]
    public int NodeOrder { get; set; }

    [JsonIgnore]
    public string Path { get; set; }

    [JsonProperty(PropertyName = "organizationRoles")]
    public List<OrganizationNodeRoleInfo> OrganizationRoles { get; set; }
}
