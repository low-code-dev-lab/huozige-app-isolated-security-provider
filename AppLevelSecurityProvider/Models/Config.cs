﻿using Newtonsoft.Json;

namespace TenantSecurityProvider.Models;

public class Config
{
    /// <summary>
    /// 存放应用ID的自定义属性名
    /// </summary>
    [JsonProperty("AppIdCustomizedPropertyName")]
    public string AppIdCustomizedPropertyName { get; set; }

    /// <summary>
    /// 应用ID
    /// </summary>
    [JsonProperty("AppId")]
    public string AppId { get; set; }

    /// <summary>
    /// 组织根节点名称
    /// </summary>
    [JsonProperty("OrganizationRootNodeName")]
    public string OrganizationRootNodeName { get; set; }
}
