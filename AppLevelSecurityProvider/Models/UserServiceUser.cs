﻿namespace TenantSecurityProvider.Models;

public class UserServiceUser
{
    public string UserName { get; set; }

    public string FullName { get; set; }

    public string Email { get; set; }

    public List<string> RoleNames { get; set; }

    public List<CustomProperty> CustomProperties  { get; set; }
}
