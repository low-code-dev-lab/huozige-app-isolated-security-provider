﻿namespace TenantSecurityProvider.Models;

public class LoginResponse
{
    public bool Result { get; set; }

    public string Message { get; set; }

    public Dictionary<string, object> Properties { get; set; }
}
