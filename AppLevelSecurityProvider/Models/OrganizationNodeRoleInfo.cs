﻿using Newtonsoft.Json;

namespace TenantSecurityProvider.Models;

/// <summary>
/// 组织节点拥有的角色信息
/// </summary>
public class OrganizationNodeRoleInfo
{
    /// <summary>
    /// 角色Id
    /// </summary>
    [JsonProperty(PropertyName = "roleId")]
    public int RoleId { get; set; }

    /// <summary>
    /// 角色名称
    /// </summary>
    [JsonProperty(PropertyName = "roleName")]
    public string RoleName { get; set; }

    /// <summary>
    /// 是否对下属生效
    /// </summary>
    [JsonProperty(PropertyName = "isEffectedForSubordinates")]
    public bool IsEffectedForSubordinates { get; set; }
}
