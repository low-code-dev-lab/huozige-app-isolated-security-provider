﻿namespace TenantSecurityProvider.Models;

public class UserInfo
{
    public string UserName { get; set; }

    public string FullName { get; set; }

    public string Email { get; set; }

    public HashSet<string> Role { get; set; }
}
